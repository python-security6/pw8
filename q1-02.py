import json
import csv


with open ("01_csv.csv", "r") as f:
    reader = csv.reader(f)
    next(reader)
    data = []
    for row in reader:
        data.append({ "firstname":row[0], "age": row[1]})


with open ("02_js.json", "w") as f:
    json.dump(data,f, indent=4)
#     writer = csv.DictWriter(f,fieldnames=fieldnames)
#     writer.writeheader()
#     for name in names:
#         writer.writerow(name)